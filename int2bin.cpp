/**
 * Alphabet = A = {\x00, \x01,...,\xFE,\xFF}
 * Language = L = A* 
 * = {\x00, \x01,...,\xFE,\xFF, \x00\x00, \x00\x01,...,\xFF\xFE,\xFF,\xFF,...}
 * 
 * This program enumerates over L.
 * It also helps demonstrate the little-endianess of the x86 system.
 * See the 'int.bin' file.  
 * 
 * Partial Output:
 * 0,0x1,0x2,0x3,...0xfd,0xfe,0xff,0,0x1,0x2,0x3,...0xfffd,0xfffe,0xffff,0,0x1,0x2,0x3,...0xfffffd,0xfffffe,0xffffff,0,0x1,0x2,0x3,
 *
*/


#include <iostream>
#include <fstream>
#include <iomanip>
#include <math.h>

using namespace std;


int main()
{
	int int_max = sizeof(int);

	ofstream intfile("int.bin", ios::binary);
 
	//first print strings of length 1, then length 2, and so on
	for(int j = 1; j <= int_max; j++) {
		for(int i = 0; i < pow(2,8 * j); i++) {
			if(i == 0 || i == 1 || i == 2 || i == 3 || i == (pow(2,8 * j)-1) 
				|| i == (pow(2,8 * j)-2) ) {
				cout << hex << showbase << i << "," << flush;
			} 
			else if(i == (pow(2,8 * j)-3) ) {
				//flush is needed because the output isn't immediately flushed to the 
				//screen for some unknown reason to me
				cout << "..." << flush;
				cout << hex << showbase << i << "," << flush;
			}
			//reinterpret_cast should normally be avoided but it helps write an int
			//to a binary file
			intfile.write(reinterpret_cast<const char *>(&i), j);
		}
	}
	
	intfile.close();
	
	return 0;
}
