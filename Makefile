.PHONY: all clean

PROG=int2bin
CPPFLAGS+=-Wall
all: ${PROG}

${PROG}: ${PROG}.cpp
	g++ ${CPPFLAGS} ${PROG}.cpp -o ${PROG}

clean: 
	-rm -f *.o ${PROG} core
